# Custom targets
Firstly, in `src/projectfile.nim:107` you will find where the various targets
are actually invoked. Targets them-selves are located in `src/targets`.

The easiest way to add a new one is to simply copy an existing one and edit it
to match, and then write your target from there, referencing
`src/targets/nim.nim` as you go.

`src/targets.nim` exists to mass-import all the various targets. You
don't strictly *have* to use this but it's considered good practice.

`src/types` is storage for any shared types to avoid any potential
circular dependencies.

`src/logging` is where common logging primitives are supposed to go,
though the vast majority of things do it their own way (perhaps something
to standardize?)
