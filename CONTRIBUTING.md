# Contributing
If you intend to contribute an target to builder, please ensure your target's
source file is stored within `src/targets` and name your source file as your
language's extension, with care taken to avoid using symbols.
(Ex: `src/targets/cpp.nim` vs `src/targets/c++.nim`)

Within this file you should provide a build function which will match the
prototype:
```nim
	proc build*(project: Config, task: string): FailState
```

For the invocation site, you should try to keep the messages generally the
same, though `types.FailState` provides `targetSpecific` which you can make
report whatever you want.

Colors are pretty much up to you, though try to keep them generally
in-line with the official nim target.

Keys are _entirely_ up to you, though they should all be
1. properly-spelled
2. in English
3. in lower-case

Please ensure your file is properly licensed under the GPL, and bears
the GPL header found in all the other source files on the first line.

Your target should be imported and exported by `src/targets.nim`, so
as to match the other existing targets.

I have absolutely no clue how to do git forks merge things, and tbqh I'm sort
of on the fence regarding whether I want to continue using git for my projects
or switch to fossil, so for now you'll want to contact me on IRC.
