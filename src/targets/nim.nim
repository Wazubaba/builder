# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import os
import osproc
import streams
import parsecfg
import strutils
import tables
import terminal

# Project imports
import ../types
import ../logging
import ../version


type Class = enum
  console,
  gui,
  lib,
  staticlib,
  unknown


proc build*(project: Config, task: string): FailState =
  # Ensure optional keys have their proper fallbacks
  let generator = if project[task].hasKey("generator"): project[task]["generator"]
                  else: "c"
  
  let sources = if project[task].hasKey("sources"): project[task]["sources"]
                else: "src/main"

  let minversion = if project[task].hasKey("minimum.nim.version"): project[task]["minimum.nim.version"]
                   else: ""
  
  var commandline = "nim " & generator


  # Test if nim is even available and grab the version number if so

  var version: VersionNumber
  
  var buffer = execProcess("nim --version")
  if buffer.len() > 2 and buffer[0..2].toLower() != "nim":
    error("No nim compiler available. Please add a nim compiler to your path")
    return aborted
  else:
    version = buffer.split("\n")[0].split()[3].parse_version_number()
    if version == (-1, -1, -1):
      error("Failed to parse nim compiler's version string!")
      return aborted

  # Process the mininmum required version

  var requiredMinimumVersion = minversion.parse_version_number()
  if requiredMinimumVersion == (-1, -1, -1) and minversion != "":
    error("Failed to parse minimum required version!")
    return aborted
  else:
    if version.version_number_lt(requiredMinimumVersion):
      styledEcho(resetStyle, styleBright, fgRed, "Build aborted! ", fgWhite, "Your version of nim is too old: ", fgYellow, version.version2String(), fgWhite, " Expected: >= ", fgYellow, requiredMinimumVersion.version2String())
      return aborted

  # Support class specification key
  if project[task].hasKey("class"):
    case project[task]["class"].toLower().strip()
    of "console":
      commandLine.add(" --app:console")
    of "gui":
      commandLine.add(" --app:gui")
    of "lib":
      commandLine.add(" --app:lib")
    of "staticlib":
      commandLine.add(" --app:staticlib")
    else:
      warn("Unknown class type: " & project[task]["class"])
    
    styledEcho(resetStyle, styleBright, fgWhite, "Building application class: ", fgCyan, project[task]["class"], resetStyle)


  # Support extra arguments to the compiler 
  if project[task].hasKey("args"):
    for arg in project[task]["args"].split(","):
      commandLine.add(" " & arg.strip())
  
  # Estabilish output-path
  let bin = if project[task].hasKey("outbin"):
              if project[task].hasKey("outdir"):
                discard existsOrCreateDir(project[task]["outdir"])
                os.joinPath(project[task]["outdir"], project[task]["outbin"])
              else:
                project[task]["outbin"]
            else: task

  commandLine.add(" " & "-o:" & bin)

  # Allow easy altering of the cache location
  if project[task].hasKey("cache"):
    commandLine.add(" --nimcache:" & project[task]["cache"])

  # Finally append sources to the command
  for file in sources.split(","):
    commandLine.add(" " & file.strip())

  styledEcho(resetStyle, styleBright, fgWhite, "Using nim version: ", fgYellow, version.version2String(), resetStyle)

  styledEcho(resetStyle, styleBright, fgWhite, "Building with commandline: ", fgYellow, commandline, resetStyle)

  return if execShellCmd(commandLine) == 0: noError
  else: failed
