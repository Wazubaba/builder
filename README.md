# B U I L D E R
#### A nim(and more?) project build system that is only half as painful

### IMPORTANT
_**This project has been improved and rewritten. You can find builder2 [here](https://gitlab.com/Wazubaba/builder2)**_


Builder is aimed at simplying build systems.
It may not be the most perfectly correct or the fastest system,
but it makes up for that by not requiring an up-hill fight the
entire time, or having to store crufty macro files to try to
cover up some of the rough edges.

Instead of using crufty proprietary language to implement the
project configuration, simple INI format is used. 

A project is broken down into the tasks needed to build it. These
tasks have a given type, which determines what build target to
use internally.

Currently there are 3 kinds of tasks:

* nim target - A nim-based project to compile
* shell target - A shell command to execute
* group target - A group of other targets to execute

For info on how the `project.ini` file is formatted, please see
Builder's very own `project.ini`. There also is documentation in
`docs/project_files.md`.

## Building
Initially, you have to use the `bootstrap.sh` script to compile builder,
but after that builder should be fully-capable of building itself, unless
a massive breaking-change is made to the nim target.

By default, the bootstrapper builds a debug version. To build a release
version:
```bash
./bootstrap.sh release
```

You can also issue `--help` to see more available commands.


## Installing
Builder is a self-contained executable and can be installed by
putting it somewhere on your path.

## Feature requests
I have no clue here. Use whatever the place you found this provides to make
a request. TBQH I doubt anyone but I will even use this tool anyways lmao.
