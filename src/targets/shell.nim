# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import os
import terminal
import parsecfg
import ../types
import tables
import ../logging
import strutils

proc build*(project: Config, task: string): FailState =
  var expected = 0

  if project[task].hasKey("expect"):
    try: expected = project[task]["expect"].parseInt()
    except ValueError:
      return aborted

  if not project[task].hasKey("command"):
    return malformed
  styledEcho(resetStyle, styleBright, fgWhite, "Executing commandline: ", fgyellow, project[task]["command"], resetStyle)
  let status = execShellCmd(project[task]["command"])

  if status == expected:
    return noError
  else:
    styledEcho(resetStyle, styleBright, fgWhite, "Command exited with status: ", fgYellow, $status, fgWhite, ", Expected: ", fgYellow, $expected, resetStyle)
    return failed
