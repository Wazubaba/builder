# 2018-07-05

* Add minimum compiler version check to nim target
* Add more fail-types for the targets to return for better info
* Add simple albeit non-reliable version helpers
* Make nim target test for nim binary and show version
