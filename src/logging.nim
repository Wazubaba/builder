# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import terminal

proc warn*(message: string) =
  styledEcho(resetStyle, styleBright, fgYellow, message, resetStyle)

proc error*(message: string) =
  styledEcho(resetStyle, styleBright, fgRed, message, resetStyle)

proc success*(message: string) =
  styledEcho(resetStyle, styleBright, fgGreen, message, resetStyle)

proc info*(message: string) =
  styledEcho(resetStyle, styleBright, fgWhite, message, resetStyle)

proc project_info*(name: string, buildType: string, description: string, author: string, license: string) =
  # Don't print anything if there is nothing to print...
  if description == "" and author == "" and license == "": return

  styledEcho(resetStyle, styleBright, "[", fgGreen, name, fgWhite, "] type: ", fgMagenta, buildType, fgWhite)

  if author != "":
    styledEcho("\t", "Author: ", fgMagenta, author, fgWhite)
  if license != "":
    styledEcho("\t", "License: ", fgCyan, license, fgWhite)
  if description != "":
    if author != "" or license != "":
      echo ""
    echo "\t", description
  styledEcho(resetStyle)
#[
import macros
import strformat
from strutils import toLowerAscii
import colors

template styledEchoProcessArg(f: File, s: string) = write f, s
template styledEchoProcessArg(f: File, style: Style) = setStyle(f, {style})
template styledEchoProcessArg(f: File, style: set[Style]) = setStyle f, style
template styledEchoProcessArg(f: File, color: ForegroundColor) =
  setForegroundColor f, color
template styledEchoProcessArg(f: File, color: BackgroundColor) =
  setBackgroundColor f, color
template styledEchoProcessArg(f: File, color: Color) =
  setTrueColor f, color
template styledEchoProcessArg(f: File, cmd: TerminalCmd) =
  when cmd == resetStyle:
    resetAttributes(f)
  when cmd == fgColor:
    fgSetColor = true
  when cmd == bgColor:
    fgSetColor = false

macro styledWriteImpl*(f: File, m: varargs[typed]): untyped =
  ## Similar to ``writeLine``, but treating terminal style arguments specially.
  ## When some argument is ``Style``, ``set[Style]``, ``ForegroundColor``,
  ## ``BackgroundColor`` or ``TerminalCmd`` then it is not sent directly to
  ## ``f``, but instead corresponding terminal style proc is called.
  ##
  ## Example:
  ##
  ## .. code-block:: nim
  ##
  ##   proc error(msg: string) =
  ##     styledWriteLine(stderr, fgRed, "Error: ", resetStyle, msg)
  ##
  let m = callsite()
  var reset = false
  result = newNimNode(nnkStmtList)

  for i in countup(2, m.len - 1):
    let item = m[i]
    case item.kind
    of nnkStrLit..nnkTripleStrLit:
      if i == m.len - 1:
        # optimize if string literal is last, just call writeLine
        result.add(newCall(bindSym"write", f, item))
        if reset: result.add(newCall(bindSym"resetAttributes", f))
        return
      else:
        # if it is string literal just call write, do not enable reset
        result.add(newCall(bindSym"write", f, item))
    else:
      result.add(newCall(bindSym"styledEchoProcessArg", f, item))
      reset = true

  #result.add(newCall(bindSym"write", f, newStrLitNode("\n")))
  if reset: result.add(newCall(bindSym"resetAttributes", f))


macro styledWrite*(args: varargs[untyped]): untyped =
  ## Echoes styles arguments to stdout using ``styledWriteLine``.
  result = newCall(bindSym"styledWriteImpl")
  result.add(bindSym"stdout")
  for arg in children(args):
    result.add(arg)
]#
