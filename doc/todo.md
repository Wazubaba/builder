* Add minimum version support options to all targets that can support them.
* Stress-test for any bugs
* Add C target
* Add c++ target
* Add nimble target for integration's sake
* Add dependency support to tasks (i.e. fail if another task fails)
