# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import projectfile
import logging
import os
import parseopt
import strutils

let VERSION = "builder 1.0.0 compiled with Nim: " & NimVersion

proc showHelp() =
  echo """
Usage: builder [options] [task1] [task2] [task3]...

Options:
    -h, --help                Show this help and exit

    -v, --version             Show version and exit

    --dryrun                  Parse the build file but don't actually
                              execute any tasks

    -p, --project-file=path   Specify project file to use (defaults to
                              project.ini)

    --recursion-limit=value   Specify the maximum depth of recursion in task
                              groups to permit (defaults to 5)

    -i, --info                Show task information as well
"""

var
  dryrun = false
  targetFile = "project.ini"
  tasks: seq[string]
  recursionLimit = 5
  infoMode = false

for kind, key, val in getOpt():
  case kind
  of cmdLongOption, cmdShortOption:
    case key
    of "help", "h":
      showHelp()
      quit()
    of "version", "v":
      echo VERSION
      quit()
    of "dryrun":
      dryrun = true
    of "recursion-limit":
      if val.isDigit:
        recursionLimit = val.parseInt()
        if recursionLimit < 0:
          warn("Recursion limit of 0 or less can lead to infinite loops, but will continue...")
    of "project-file", "p":
      targetFile = val
    of "info", "i":
      infoMode = true
    else:
      discard
  else:
    tasks.add(key)

if tasks.len() == 0: tasks = @["defaults"]

projectfile.parse(targetFile, dryrun, tasks, recursionLimit, infoMode)
