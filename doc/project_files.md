# Project.ini format

# Preface
Each group (`[something_like_this]`) is considered a specific
task.

Each task _must_ have a `type` key within it, which will denote the
type of task it is and which target to use. Please note that task names are case-sensitive.


# Nim
With the `nim` type, the target will be built as a nim project.

The following keys are available:

| Key                 | Description                                                                                                                                | Default                   |
|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------|---------------------------|
| desc                | Description of the project (shown with the `--info` option)                                                                                | N/A                       |
| author              | Author of the project (shown with the `--info` option                                                                                      | N/A                       |
| license             | License of the project (shown with the `--info` option                                                                                     | N/A                       |
| generator           | What generator back-end nim should use                                                                                                     | c                         |
| cache               | Where the nimcache should be placed                                                                                                        | Omitted from command-line |
| sources             | Comma-separated list of source files that should be used (With nim you only need to provide your main file unless it is a library project) | src/main.nim              |
| outdir              | Directory to put the generated executable into, provided outbin is also set                                                                | Omitted from command-line |
| outbin              | Name of the binary to output                                                                                                               | Uses the name of the Task |
| minimum.nim.version | Minimum version of the Nim compiler required to build                                                                                      | Doesn't bother checking   |
| args                | Comma-separated list of arguments to pass to the compiler                                                                                  | Omitted from command-line |
| class               | class to pass to the compiler (console, gui, lib, or staticlib)                                                                            | Omitted from command-line |


# Group
With the `group` type, the task will be recursively scanned for substasks,
and a list of them will then be built. This task has only one key: `subtasks`.

This key should contain a comma-separated list of tasks to process. You can
have multiple nested group tasks, but you might need to pass the
`--recursion-limit=XXX` flag to Builder if you have a lot. Builder will inform
you if the recursion limit is reached.


# Shell
With the `shell` type, the task will be executed using nim's `os.execShellCmd`
function.

The following keys are available:

| Key     | Description                                                 | Default  |
|---------|-------------------------------------------------------------|----------|
| desc    | Description of the project (shown with the `--info` option) | N/A      |
| author  | Author of the project (shown with the `--info` option       | N/A      |
| license | License of the project (shown with the `--info` option      | N/A      |
| expect  | What result to expect from the executed command             | 0        |
| command | What command-line to execute                                | REQUIRED |
