# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import parsecfg
import types
import logging
import types
import targets
import tables
import terminal
import strutils
import sequtils

var RECURSION_LEVEL = 0

# If I knew how to forcibly inline in nim this would be inlined
proc is_task_list(project: Config, task: string): bool =
  if not project[task].hasKey("type"): return false
  result = project[task]["type"] == "group"
      
# Recursively iterate untill we have added every task within any potentially
# nested groups
proc parse_task_list(project: Config, task: string, recursionLimit: int = 5): seq[string] =
  #result = newSeq[string]()
  if project[task].hasKey("subtasks"):
    for item in project[task]["subtasks"].split(","):
      let key = item.strip()
      if project.hasKey(key) and is_task_list(project, key):
        if RECURSION_LEVEL <= recursionLimit:
          inc RECURSION_LEVEL
          result = concat(result, parse_task_list(project, key, recursionLimit))
        else:
          warn("Recursion limit reached")
      else:
        result.add(key)
  else:
    return @[task]

# Parse the ini. As each project is found call the associated target.
proc parse*(path: string = "project.ini", dryrun: bool = false, initialtasks: seq[string] = @["defaults"], recursionLimit: int = 5, infoMode: bool = false) =

  var project: Config

  try:
    project = loadConfig(path)
  except IOError:
    styledEcho(resetStyle, styleBright, fgRed, "Cannot load project file: ", fgWhite, path, resetStyle)
    quit(1)

  info("Using project file: " & path)

  var tasks: seq[string]

  # Stage 1: Figure out all the tasks we have to build
  for target in initialtasks:
    if project.hasKey(target):
      if project[target].hasKey("type"):
        if is_task_list(project, target):
          tasks = parse_task_list(project, target, recursionLimit)
        else:
          tasks.add(target)
      else:
        styledEcho(resetStyle, styleBright, fgWhite, "Malformed entry for ", fgRed, target, resetStyle)
  #      if project[target].hasKey("subtask"):
   #       for item in project[target]["subtask"].split(","):
    #        tasks.add(item.strip())
 #       else:
  #        warn("Skipping task " & target)
    else:
      tasks.add(target)

  tasks = tasks.deduplicate()
  # Stage 2: Output list of targets that will be built.
  info("Building target(s):")
  var fail = false;
  var validTasks: seq[string]

  for target in tasks:
    if not project.hasKey(target):
      fail = true
      styledEcho(resetStyle, styleBright, fgRed, "\t", target, fgYellow, " - Not defined!", resetStyle)
    else:
      if not project[target].hasKey("type"):
        styledEcho(resetStyle, styleBright, fgRed, "\t", target, fgYellow, " - Missing type info!", resetStyle)
        continue
      styledEcho(resetStyle, styleBright, fgGreen, "\t" & target, resetStyle)
      if infoMode:
        let desc = if project[target].hasKey("desc"): project[target]["desc"] else: ""
        let author = if project[target].hasKey("author"): project[target]["author"] else: ""
        let license = if project[target].hasKey("license"): project[target]["license"] else: ""
        project_info(target, project[target]["type"], desc, author, license)
      validTasks.add(target.strip())

  if validTasks.len() == 0:
    error("No valid tasks found!")
    quit(0)

  if dryrun:
    styledEcho(resetStyle, styleBright, fgMagenta, "Exiting due to dryrun mode", resetStyle)
    quit(0)

  if fail:
    styledEcho(resetStyle, styleBright, fgWhite, "Not all targets are properly configured and will be skipped, continue anyways? [", styleDim, "y", resetStyle, styleBright, "/N]: ", resetStyle)
    let choice = readLine(stdin)
    if not(choice.toLower() in ["y", "ye", "yes"]):
      info("Build aborted by user")
      quit(0)
  
  # Now we build the tasks that we actually have available
  for task in validTasks:
    # We assume now that all tasks found are valid enough to be handed
    # to their respective target builders.
    case project[task]["type"]
    of "group":
      warn("Somehow a group task slipped through here?")

    of "nim":
      styledEcho(resetStyle, styleBright, fgWhite, "Building nim task: ", fgCyan, task, resetStyle)

      case targets.nim.build(project, task)
      of malformed:
        styledEcho(resetStyle, styleBright, fgRed, "ERROR IN PROJECT CONFIGURATION", resetStyle)
        error("\Task is missing required properties")
      of failed:
        styledEcho(resetStyle, styleBright, fgRed, "FAILED", resetStyle)
        error("\tBuild failed with non-zero exit status")
      of aborted:
        styledEcho(resetStyle, styleBright, fgRed, "ABORTED", resetStyle)
        error("\tBuild aborted due to pre-processing issues")
      of targetSpecific:
        styledEcho(resetStyle, styleBright, fgYellow, "...Wut?", resetStyle)
        warn("How did you make this happen? Please report this and provide your project file and a brief description of wtf you were doing")
      of noError:
        success("SUCCESS")

    of "shell":
      styledEcho(resetStyle, styleBright, fgWhite, "Performing shell task ", fgCyan, task, resetStyle)

      case targets.shell.build(project, task)
      of malformed:
        styledEcho(resetStyle, styleBright, fgRed, "ERROR IN PROJECT CONFIGURATION", resetStyle)
        error("\tCommand is malformed")
      of failed:
        styledEcho(resetStyle, styleBright, fgRed, "FAILED", resetStyle)
        error("\tCommand status did not match expected result")
      of aborted:
        styledEcho(resetStyle, styleBright, fgRed, "ABORTED", resetStyle)
        error("\tBuild aborted due to pre-processing issues")
      of targetSpecific:
        styledEcho(resetStyle, styleBright, fgYellow, "...Wut?", resetStyle)
        warn("How did you make this happen? Please report this and provide your project file and a brief description of wtf you were doing")
      of noError:
        success("SUCCESS")
    else:
      warn("Task " & task & " is of an unknown type and will be skipped.")
